from typing import Union
import joblib
from fastapi import FastAPI
from pydantic import BaseModel
import numpy as np
import pandas as pd
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Form
# from sklearn.preprocessing import StandardScaler

app = FastAPI()
origins =[
    "http://localhost:5173"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def add_feature(X, feature_to_add):

    from scipy.sparse import csr_matrix, hstack
    return hstack([X, csr_matrix(feature_to_add).T], 'csr')

model = joblib.load('CustomerChrunfinalmodel.pkl')
scaler = joblib.load('scaler.pkl')
answer = ['Not', 'Yes']
class Customer(BaseModel):
    Age: float
    Gender: float
    Tenure: float
    Usage_Frequency: float
    Support_Calls: float
    Payment_Delay: float
    Subscription_Type: float
    Contract_Length: float
    Total_Spend	: float
    Last_Interaction: float


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.put("/Customer")
async def update_item(item: Customer):
    data = np.array(
    [
        [item.Age, item.Gender, item.Tenure, item.Usage_Frequency, item.Support_Calls, item.Payment_Delay, item.Subscription_Type, item.Contract_Length, item.Total_Spend, item.Last_Interaction]
    ])
    # scaler = StandardScaler()
    # scaler.fit(data)
    data = scaler.transform(data)
    prediction = model.predict(data)
    print(answer[prediction[0]])

    return {"prediction" : answer[prediction[0]]}


